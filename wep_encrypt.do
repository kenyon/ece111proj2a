vlog wep_encrypt.v wep_test.v
vsim wep_test
radix -hex
add wave sim:/wep_test/clk
add wave sim:/wep_test/nreset
add wave -radix unsigned sim:/wep_test/plain_addr
add wave -radix unsigned sim:/wep_test/cipher_addr
add wave -radix unsigned sim:/wep_test/frame_size
add wave -radix unsigned sim:/wep_test/start_encrypt
add wave sim:/wep_test/seed_msw
add wave sim:/wep_test/seed_lsw
add wave sim:/wep_test/port_A_data_out
add wave sim:/wep_test/port_A_clk
add wave sim:/wep_test/port_A_data_in
add wave -radix unsigned sim:/wep_test/port_A_addr
add wave sim:/wep_test/port_A_we
add wave sim:/wep_test/done
run 100000
